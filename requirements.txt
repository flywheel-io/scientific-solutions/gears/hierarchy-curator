annotated-types==0.7.0 ; python_version >= "3.9" and python_version < "4.0"
anyio==4.6.0 ; python_version >= "3.9" and python_version < "4.0"
attrs==24.2.0 ; python_version >= "3.9" and python_version < "4.0"
backoff==2.2.1 ; python_version >= "3.9" and python_version < "4.0"
certifi==2024.8.30 ; python_version >= "3.9" and python_version < "4.0"
charset-normalizer==3.3.2 ; python_version >= "3.9" and python_version < "4.0"
dicom-validator==0.3.5 ; python_version >= "3.9" and python_version < "4.0"
dotty-dict==1.3.1 ; python_version >= "3.9" and python_version < "4.0"
exceptiongroup==1.2.2 ; python_version >= "3.9" and python_version < "3.11"
flywheel-gear-toolkit==0.6.18 ; python_version >= "3.9" and python_version < "4.0"
flywheel-gears==0.3.1 ; python_version >= "3.9" and python_version < "4.0"
flywheel-sdk==19.1.0 ; python_version >= "3.9" and python_version < "4.0"
fw-client==0.8.6 ; python_version >= "3.9" and python_version < "4.0"
fw-file==3.4.0 ; python_version >= "3.9" and python_version < "4.0"
fw-http-client==1.7.2 ; python_version >= "3.9" and python_version < "4.0"
fw-meta==4.2.2 ; python_version >= "3.9" and python_version < "4.0"
fw-utils==5.0.3 ; python_version >= "3.9" and python_version < "4.0"
h11==0.14.0 ; python_version >= "3.9" and python_version < "4.0"
h2==4.1.0 ; python_version >= "3.9" and python_version < "4.0"
hpack==4.0.0 ; python_version >= "3.9" and python_version < "4.0"
httpcore==1.0.5 ; python_version >= "3.9" and python_version < "4.0"
httpx[http2]==0.27.2 ; python_version >= "3.9" and python_version < "4.0"
hyperframe==6.0.1 ; python_version >= "3.9" and python_version < "4.0"
idna==3.10 ; python_version >= "3.9" and python_version < "4.0"
jsonschema-specifications==2023.12.1 ; python_version >= "3.9" and python_version < "4.0"
jsonschema==4.23.0 ; python_version >= "3.9" and python_version < "4.0"
lxml==4.9.4 ; python_version >= "3.9" and python_version < "4.0"
natsort==8.4.0 ; python_version >= "3.9" and python_version < "4.0"
nibabel==5.2.1 ; python_version >= "3.9" and python_version < "4.0"
numpy==1.26.4 ; python_version >= "3.9" and python_version < "4.0"
packaging==24.1 ; python_version >= "3.9" and python_version < "4.0"
pandas==2.2.3 ; python_version >= "3.9" and python_version < "4.0"
piexif==1.1.3 ; python_version >= "3.9" and python_version < "4.0"
pillow==10.4.0 ; python_version >= "3.9" and python_version < "4.0"
pydantic-core==2.23.4 ; python_version >= "3.9" and python_version < "4.0"
pydantic-settings==2.5.2 ; python_version >= "3.9" and python_version < "4.0"
pydantic==2.9.2 ; python_version >= "3.9" and python_version < "4.0"
pydicom==2.4.4 ; python_version >= "3.9" and python_version < "4.0"
pypng==0.20220715.0 ; python_version >= "3.9" and python_version < "4.0"
python-dateutil==2.9.0.post0 ; python_version >= "3.9" and python_version < "4.0"
python-dotenv==1.0.1 ; python_version >= "3.9" and python_version < "4.0"
pytz==2024.2 ; python_version >= "3.9" and python_version < "4.0"
pyyaml==6.0.2 ; python_version >= "3.9" and python_version < "4.0"
referencing==0.35.1 ; python_version >= "3.9" and python_version < "4.0"
requests-toolbelt==1.0.0 ; python_version >= "3.9" and python_version < "4.0"
requests==2.32.3 ; python_version >= "3.9" and python_version < "4.0"
rfc3987==1.3.8 ; python_version >= "3.9" and python_version < "4.0"
rpds-py==0.20.0 ; python_version >= "3.9" and python_version < "4.0"
six==1.16.0 ; python_version >= "3.9" and python_version < "4.0"
sniffio==1.3.1 ; python_version >= "3.9" and python_version < "4.0"
typing-extensions==4.12.2 ; python_version >= "3.9" and python_version < "4.0"
tzdata==2024.2 ; python_version >= "3.9" and python_version < "4.0"
tzlocal==5.2 ; python_version >= "3.9" and python_version < "4.0"
urllib3==2.2.3 ; python_version >= "3.9" and python_version < "4.0"
