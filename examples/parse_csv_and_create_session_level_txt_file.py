"""An example curation script that parse a CSV file.

It finds the corresponding subject and session and store some text columns as .txt
files at the session level.
"""

import logging
import os
import sys

import flywheel
import pandas as pd
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger(__name__)

# Columns mapping from CSV to Flywheel
SUBJECT_LABEL_COLUMN = "PatientID"
SESSION_LABEL_COLUMN = "AccessionNumber"

# Columns to write as text file
COLUMNS_TO_WRITE = [
    ("Text", "report.txt"),
    ("PositiveFindings", "positive-findings.txt"),
]


class Curator(HierarchyCurator):
    """A Curator class that parse a CSV file and curate a project accordingly."""

    def __init__(self, **kwargs):
        """Initialize the curator."""
        super().__init__(**kwargs)
        # Stop at project level since we only care about curating project
        self.config.stop_level = "project"
        if self.additional_input_one:
            self.data = pd.read_csv(self.additional_input_one)
        else:
            log.error("No csv file provided as additional-input-one")
            sys.exit(1)

        # some bit of validation
        if not all(
            [
                c in self.data.columns
                for c in [SUBJECT_LABEL_COLUMN, SESSION_LABEL_COLUMN]
            ]
        ):
            log.error(
                f"Input CSV file does not contain the required columns:"
                f"{SUBJECT_LABEL_COLUMN} and {SESSION_LABEL_COLUMN}"
            )
            sys.exit(1)
        if not all([c in self.data.columns for c, _ in COLUMNS_TO_WRITE]):
            log.error(
                f"Input CSV file does not contain the required columns: "
                f"{COLUMNS_TO_WRITE.keys()}"
            )
            sys.exit(1)
        if any(
            self.data.duplicated(subset=[SESSION_LABEL_COLUMN, SUBJECT_LABEL_COLUMN])
        ):
            log.error("Input CSV file contains duplicated rows")
            sys.exit(1)

    def curate_project(self, project: flywheel.Project):
        """Curate a project."""
        view = self.client.View(columns="session")
        df = self.client.read_view_dataframe(view, project.id)

        for i, row in df.iterrows():
            match = self.data[
                (self.data[SUBJECT_LABEL_COLUMN] == row["subject.label"])
                & (self.data[SESSION_LABEL_COLUMN] == row["session.label"])
            ]
            if match.empty:
                log.debug(
                    f"Skipping {row['subject.label']}/{row['session.label']}, "
                    f"no match found in project"
                )
                continue

            # Create text files with the content of the column defined in
            # COLUMNS_TO_WRITE and store them at the session level
            session = self.client.get(row["session.id"])
            log.info(
                f"Found match for {row['subject.label']}/{row['session.label']}, "
                f"Uploading reports"
            )
            for c, f in COLUMNS_TO_WRITE:
                with open(f, "w") as fp:
                    fp.write(match[c].iloc[0])
                session.upload_file(f)
                os.remove(f)
