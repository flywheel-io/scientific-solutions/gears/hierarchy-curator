"""A Hierarchy curator script to split in train-validation-test mlset.

It uses scikit-learn's train_test_split (
https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html#train-test-split
) to split the Subjects in a Project into Train, Test and Validation sets
to be used in ML model training, and set the corresponding assigned value
to the Subject's 'mlset' metadata.

The script outputs a CSV file listing the Subject's label, id and mlset value
for each Subject in the Project.

The relative sizes of the sets are defined as globals, and can be modified
as needed (noting that they have to add up to 1.0).
"""

import csv
import logging

from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger("my_curator")
log.setLevel("DEBUG")

RANDOM_STATE = 42
TRAIN = 0.70
VALID = 0.2
TEST = 0.1
SCALE = 12

if round(TRAIN + VALID + TEST, 2) != 1.0:
    raise ValueError("TRAIN, VALID, TEST must sum to 1.0.")


def split_subjects(subjects):
    """Split subjects into train, validation, and test sets."""
    from sklearn.model_selection import train_test_split

    strain, stest = train_test_split(
        subjects, test_size=round(1 - TRAIN, SCALE), random_state=RANDOM_STATE
    )
    sval, stest = train_test_split(
        stest,
        test_size=round(1 - VALID / (TEST + VALID), SCALE),
        random_state=RANDOM_STATE,
    )
    return strain, sval, stest


class Curator(HierarchyCurator):
    """A Hierarchy curator to split in train-validation-test mlset."""

    def __init__(self, **kwargs):
        """Initialize the Curator."""
        super().__init__(**kwargs, extra_packages=["scikit-learn"])

    def curate_project(self, project):
        """Curate the project."""
        subjects = project.subjects()
        log.info("Splitting...")
        strain, sval, stest = split_subjects(subjects)
        out_path = str(self.context.output_dir / "output.csv")
        with open(out_path, "w", newline="") as myfile:
            writer = csv.DictWriter(myfile, ["Label", "ID", "Split"])
            writer.writeheader()

            log.info("Setting training...")
            for s in strain:
                s.update(mlset="Training")
                writer.writerow({"Label": s.label, "ID": s.id, "Split": "training"})
            log.info("Setting validation...")
            for s in sval:
                s.update(mlset="Validation")
                writer.writerow({"Label": s.label, "ID": s.id, "Split": "validation"})
            log.info("Setting testing ...")
            for s in stest:
                s.update(mlset="Testing")
                writer.writerow({"Label": s.label, "ID": s.id, "Split": "testing"})
