"""An example curation script to retrigger gear rules based on fiddling file.type."""

import logging

import flywheel
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger()
log.setLevel("DEBUG")


def retrigger(acquisition, file: flywheel.FileEntry):
    """Retrigger gear rules by changing file.type and then changing it back."""
    type_ = file.type
    file = file.reload()
    file._parent = acquisition
    file.update(type="something-else")
    # clearing all metadata on file
    for t in file.tags:
        file.delete_tag(t)
    file.replace_classification({})
    file.replace_info({})
    file.update(type=type_)


class Curator(HierarchyCurator):
    """A curation script to retrigger gear rules based on fiddling file.type."""

    def curate_acquisition(self, acquisition: flywheel.Acquisition):
        """Curate an acquisition."""
        log.info(f"Curating acquisition {acquisition.label}")
        for file_ in acquisition.files:
            if file_.type == "dicom":
                log.info(f"Retriggering {file_.name}")
                retrigger(acquisition, file_)
