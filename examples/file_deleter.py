"""A script to delete files attached at the acquisition level and tagged with 'delete'."""

import logging

import flywheel
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger(__name__)


def remove_dtagged_files(self, acq):
    """Remove files tagged with 'delete' from the acquisition."""
    files_rm = []
    if (
        hasattr(acq, "files")
        and (acq.container_type != "analysis")
        and acq.parents.session
    ):
        for f in acq.files:
            if "delete" in f.tags:
                log.info(
                    f" Deleting file {f.name} file ID {f._id} from acquisition ID {acq.id} "
                )
                files_rm.append(f.name)
                acq.delete_file(f.name)
    return files_rm


class Curator(HierarchyCurator):
    """A curation script to delete files attached at the acquisition level."""

    def __init__(self, **kwargs):
        """Initialize the curator."""
        super().__init__(**kwargs)
        self.config.stop_level = "acquisition"

    def curate_acquisition(self, acquisition: flywheel.Acquisition):
        """Curate an acquisition."""
        remove_dtagged_files(self, acquisition)
