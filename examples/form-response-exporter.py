"""Export form responses from a project."""

import datetime
import logging
from functools import lru_cache

import pandas as pd
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger()

MAP_FORMRESPONSE_KEY_TO_QUESTION = False

TASK_TYPE_MAPPING = {"R": "Read"}


@lru_cache(maxsize=512)
def get_task(client, task_id):
    """Get a task from the API."""
    return client.get(f"/api/readertasks/{task_id}")


@lru_cache(maxsize=512)
def get_protocol(client, protocol_id):
    """Get a protocol from the API."""
    return client.get(f"/api/read_task_protocols/{protocol_id}")


@lru_cache(maxsize=512)
def get_form(client, form_id):
    """Get a form from the API."""
    return client.get(f"/api/forms/{form_id}")


@lru_cache(maxsize=512)
def get_project(client, project_id):
    """Get a project from the API."""
    return client.get(f"/api/projects/{project_id}")


@lru_cache(maxsize=512)
def get_subject(client, subject_id):
    """Get a subject from the API."""
    return client.get(f"/api/subjects/{subject_id}")


@lru_cache(maxsize=512)
def get_session(client, session_id):
    """Get a session from the API."""
    return client.get(f"/api/sessions/{session_id}")


@lru_cache(maxsize=512)
def get_acquisition(client, acquisition_id):
    """Get an acquisition from the API."""
    return client.get(f"/api/acquisitions/{acquisition_id}")


@lru_cache(maxsize=512)
def get_file(client, file_id):
    """Get a file from the API."""
    return client.get(f"/api/files/{file_id}")


class Curator(HierarchyCurator):
    """A curation script to export form responses from a project."""

    def __init__(self, *args, **kwargs):
        """Initialize the curator."""
        super(Curator, self).__init__(*args, extra_packages=["fw-client"], **kwargs)

    def curate_project(self, project):  # noqa: PLR0912, PLR0915
        """Curate the project."""
        from fw_client import FWClient
        from fw_client.errors import NotFound

        client = FWClient(api_key=self.context.get_input("api-key").get("key"))

        log.info("Getting form responses")
        # form_responses = client.get("/api/formresponses")
        form_responses = client.get(
            "/api/formresponses", params={"filter": f"parents.project={project.id}"}
        )

        df = pd.json_normalize(form_responses.get("results"))
        prefix = "response_data"
        selected_columns = [col for col in df.columns if col.startswith(prefix)]

        # Keep columns that do not have the specific prefix
        other_columns = [col for col in df.columns if not col.startswith(prefix)]

        # Melt the DataFrame to create key-value pairs
        melted_df = pd.melt(
            df,
            id_vars=other_columns,
            value_vars=selected_columns,
            var_name="question",
            value_name="answer",
        )

        melted_df.dropna(subset=["answer"], inplace=True)
        melted_df["question"] = melted_df["question"].apply(
            lambda x: ".".join(x.split(prefix + ".")[1:])
        )
        melted_df.sort_values(by=["task_id"], inplace=True)

        melted_df["session_url"] = melted_df["parents.session"].apply(
            lambda x: f"{client.baseurl}/#/sessions/{x}"
        )

        log.info("Adding task and protocol information")
        for task_id in melted_df["task_id"].unique():
            try:
                task = get_task(client, task_id)
            except NotFound:
                log.info(f"Task {task_id} not found")
                continue
            protocol = get_protocol(client, task["protocol_id"])
            if MAP_FORMRESPONSE_KEY_TO_QUESTION:
                form = get_form(client, task["form_id"])
                key_question = {
                    c.get("key"): c.get("label")
                    for c in form["form"]["studyForm"]["components"]
                }
                melted_df.loc[melted_df["task_id"] == task_id, "question"] = (
                    melted_df.loc[
                        melted_df["task_id"] == task_id, "question"
                    ].apply(lambda x: key_question.get(x, x))
                )
            melted_df.loc[melted_df["task_id"] == task_id, "protocol.label"] = protocol[
                "label"
            ]
            melted_df.loc[melted_df["task_id"] == task_id, "task.type"] = task[
                "task_type"
            ]
            melted_df.loc[melted_df["task_id"] == task_id, "task.origin"] = task[
                "origin"
            ]["id"]
            melted_df.loc[melted_df["task_id"] == task_id, "task.status"] = task[
                "status"
            ]
            melted_df.loc[melted_df["task_id"] == task_id, "task.due_date"] = task[
                "due_date"
            ]
            melted_df.loc[melted_df["task_id"] == task_id, "task.task_id"] = task[
                "task_id"
            ]
            melted_df.loc[melted_df["task_id"] == task_id, "task.assignee"] = task[
                "assignee"
            ]

        log.info("Adding hierarchy labels")
        for i, row in melted_df.iterrows():
            melted_df.loc[i, "project.label"] = (
                get_project(client, row["parents.project"]).get("label")
                if row["parents.project"]
                else None
            )
            melted_df.loc[i, "group"] = (
                get_project(client, row["parents.project"]).get("group")
                if row["parents.project"]
                else None
            )

            try:
                melted_df.loc[i, "subject.label"] = (
                    get_subject(client, row["parents.subject"]).get("label")
                    if row["parents.subject"]
                    else None
                )
            except NotFound:
                log.info(f"Subject {row['parents.subject']} not found")
                melted_df.loc[i, "subject.label"] = "NotFound"

            try:
                melted_df.loc[i, "session.label"] = (
                    get_session(client, row["parents.session"]).get("label")
                    if row["parents.session"]
                    else None
                )
            except NotFound:
                log.info(f"Session {row['parents.session']} not found")
                melted_df.loc[i, "session.label"] = "NotFound"

            try:
                melted_df.loc[i, "acquisition.label"] = (
                    get_acquisition(client, row["parents.acquisition"]).get("label")
                    if row["parents.acquisition"]
                    else None
                )
            except NotFound:
                log.info(f"Acquisition {row['parents.acquisition']} not found")
                melted_df.loc[i, "acquisition.label"] = "NotFound"

            try:
                melted_df.loc[i, "file.name"] = (
                    get_file(client, row["parents.file"]).get("name")
                    if row["parents.file"]
                    else None
                )
            except NotFound:
                log.info(f"File {row['parents.file']} not found")
                melted_df.loc[i, "file.name"] = "NotFound"

        melted_df["task.type"] = melted_df["task.type"].apply(
            lambda x: TASK_TYPE_MAPPING.get(x, "Unknown")
        )

        cols_mapping = {
            "task.task_id": "Task ID",
            "task.type": "Task Type",
            "protocol.label": "Protocol Name",
            "group": "Group",
            "project.label": "Project Label",
            "subject.label": "Subject Label",
            "session.label": "Session Label",
            "acquisition.label": "Acquisition Label",
            "file.name": "File Name",
            "task.assignee": "Task Assignee",
            "task.origin": "Task Creator",
            "task.due_date": "Task Due Date",
            "task.status": "Task Status",
            "question": "Question",
            "answer": "Answer",
            "created": "Created",
            "modified": "Modified",
            "revision": "Revision",
            "session_url": "URL",
            "parents.group": "group",
            "parents.project": "project.id",
            "parents.subject": "subject.id",
            "parents.session": "session.id",
            "parents.acquisition": "acquisition.id",
            "parents.file": "file.file_id",
            "_id": "formresponse.id",
            "form_id": "form.id",
            "task_id": "task.id",
        }
        melted_df = melted_df[cols_mapping.keys()]
        melted_df.rename(columns=cols_mapping, inplace=True)

        log.info("Exporting form responses to CSV")
        now = datetime.datetime.now()
        formatted_date = now.strftime("%Y-%m-%d_%H-%M-%S")
        output_filename = f"form-responses-{formatted_date}.csv"
        melted_df.to_csv(self.context.output_dir / output_filename, index=False)
