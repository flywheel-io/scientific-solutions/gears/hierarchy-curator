"""A curation script to batch run mask-exporter gear.

Batch run the  mask-exporter gear if the task meets the following requirements:
- The task status is marked as `Complete`
- The task parent container is a `File` container
- The task is associate with a `dicom` file type.
"""

import dataclasses
import logging
import urllib
from functools import lru_cache
from typing import Any, Dict, Optional

import backoff
import flywheel
from flywheel_gear_toolkit.utils.curator import GearToolkitContext, HierarchyCurator
from flywheel_gear_toolkit.utils.reporters import BaseLogRecord


@dataclasses.dataclass
class Log(BaseLogRecord):
    """Log record for the project export."""

    container_type: str = ""
    container_id: str = ""
    file_id: str = ""
    job_id: str = ""
    msg: str = ""
    resolved: bool = False


log = logging.getLogger("batch_run_gear")
log.setLevel("DEBUG")


def get_api_key_from_client(fw_client: flywheel.Client) -> str:
    """Returns the api key from an instance of flywheel.Client.

    Args:
        fw_client (flywheel.Client): an instance of the flywheel client

    Returns:
        (str): the api key
    """
    config = fw_client._fw.api_client.configuration
    # Get host and parse host address
    addr = urllib.parse.urlparse(config.host)
    key = config.api_key["Authorization"]

    # Return the hostname:port joined with key secret
    return ":".join([addr.netloc, key])


@backoff.on_exception(backoff.expo, flywheel.rest.ApiException, max_time=300)
def launch_gear(gear, inputs, config=None, destination=None):
    """Launch a gear."""
    return gear.run(inputs=inputs, config=config, destination=destination)


def setup_client(api_key):
    """Setup the core client."""
    from fw_core_client import CoreClient

    core_client = CoreClient(
        api_key=api_key, client_name="curator", client_version="1.0"
    )
    return core_client


@lru_cache(maxsize=512)
def get_reader_tasks_list(client, project_id):
    """Get tasks from api by project_id."""
    return client.get(f"/api/readertasks/project/{project_id}")


class Curator(HierarchyCurator):
    """Curator class to batch run mask-exporter gear."""

    def __init__(
        self, context: GearToolkitContext = None, **kwargs: Optional[Dict[str, Any]]
    ) -> None:
        """Initialize the curator."""
        super().__init__(context, extra_packages=["fw-core-client==1.1.2"], **kwargs)
        self.api_key = get_api_key_from_client(self.client)
        self.core_client = setup_client(self.api_key)
        self.gear_doc = self.client.lookup("gears/mask-exporter")
        self.gear_config = {"debug": True}
        self.config.stop_level = "project"
        self.config.report = True
        self.config.format = Log

    def curate_project(self, project: flywheel.Project):
        """Curate the project."""
        task_list = get_reader_tasks_list(self.core_client, project.id)
        if task_list.count == "0":
            log.info(f"No reader tasks associated in the {project.label} project.")
        else:
            for task in task_list.results:
                task_status = task.get("status")
                # check for task status
                if task_status == "Complete":
                    # prepare input file for launching a new job
                    parent_cont = task.get("parent")
                    if parent_cont.get("type") != "file":
                        log.warning(
                            f"Task id: {task.get('_id')} is not associated with a file. "
                            f"Unable to process."
                        )
                        self.reporter.append_log(
                            container_type="task",
                            container_id=task.get("_id"),
                            msg=f"Task id: {task.get('_id')} is not associated with a file. ",
                            file_id="",
                            job_id="",
                            resolved=False,
                        )
                    else:
                        file_id = parent_cont.get("id")
                        file_ = self.client.get_file(file_id)
                        acq_id = file_.get("parent_ref").get("id")

                        if file_.type == "dicom":
                            acq_ = self.client.get_acquisition(acq_id)
                            inputs = {"input-file": file_}
                            job_id = launch_gear(
                                self.gear_doc,
                                inputs,
                                config=self.gear_config,
                                destination=acq_,
                            )
                            self.reporter.append_log(
                                container_type="task",
                                container_id=task.get("_id"),
                                file_id=file_id,
                                job_id=job_id,
                                resolved=True,
                                msg="",
                            )
                        else:
                            log.info(f"Found {file_.type} file. Skipping....")
