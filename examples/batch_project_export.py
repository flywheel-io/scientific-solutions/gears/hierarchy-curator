"""Run project export on a list of projects."""

import dataclasses
import logging
from datetime import datetime
from typing import Any, Dict, Optional

import flywheel
from flywheel import ApiException
from flywheel_gear_toolkit.utils.curator import HierarchyCurator
from flywheel_gear_toolkit.utils.reporters import BaseLogRecord

log = logging.getLogger()

# List of projects identified by their <group.id>/<project.label> lookup string
PROJECT_LIST = [
    "<YOUR_GROUP>/<YOUR_PROJECT>",
    # ...
]

# Label of the storage target as shown in the UI (Admin / Interfaces / External Storage)
STORAGE_TARGET = "GCP fw-nicolas root"

# Payload template for the export.
# You can define in there the path that will be used for the project export as well
# include/exclude rules and other criteria. See the documentation for more details:
# https://flywheel-io.gitlab.io/tools/app/xfer/#tag/Exports/operation/create_export
PAYLOAD = {
    "label": None,
    "description": "Export triggered by hierarchy curator script",
    "refs": {"project": None, "storage": None},
    "overwrite_existing": "auto",
    "delete_extra": False,
    "ignore_conflicts": False,
    "rules": [
        {
            "level": "acquisition",
            "path": "{project.label}/{subject.label}/{session.label}/{acquisition.label}/{file.name}",
            "include": [],
            "exclude": [],
            "unzip": True,
            "unzip_path": "original",
        }
    ],
}


def setup_core_client(api_key):
    """Setup the core client."""
    from fw_core_client import CoreClient

    core_client = CoreClient(
        api_key=api_key, client_name="curator", client_version="1.0"
    )
    return core_client


@dataclasses.dataclass
class Log(BaseLogRecord):
    """Log record for the project export."""

    project_lu: str = ""
    project_id: str = ""
    storage_id: str = ""
    export_id: str = ""
    export_label: str = ""
    created: str = ""


def get_api_key_from_client(fw_client: flywheel.Client) -> str:
    """Returns the api key from an instance of flywheel.Client.

    Args:
        fw_client (flywheel.Client): an instance of the flywheel client

    Returns:
        (str): the api key
    """
    site_url = fw_client.get_config().site.get("api_url")  # Get the URL
    site_base = site_url.rsplit("/", maxsplit=1)[0]  # Remove the "/api"
    site_base = site_base.split("/")[-1]  # remove the "http://"
    key_string = fw_client.get_current_user().api_key.key
    api_key = ":".join([site_base, key_string])
    return api_key


def get_storage_id(core_client, storage_name):
    """Returns the storage id from a storage name."""
    storages = core_client.get("/xfer/storages")
    for storage in storages.get("results", []):
        if storage["label"] == storage_name:
            if storage["status_check"] != "success":
                raise Exception(
                    f"Storage {storage_name} found but not available. Check status."
                )
            return storage["_id"]
    raise Exception(f"Storage {storage_name} not found")


class Curator(HierarchyCurator):
    """Curator to export a list of projects."""

    def __init__(self, **kwargs: Optional[Dict[str, Any]]) -> None:
        """Initialize the curator."""
        super().__init__(extra_packages=["fw-core-client==1.1.2"], **kwargs)
        self.api_key = get_api_key_from_client(self.client)
        self.core_client = setup_core_client(self.api_key)
        self.stop_level = "project"
        self.config.report = True
        self.config.format = Log
        self.config.workers = 1
        self.storage_id = get_storage_id(self.core_client, STORAGE_TARGET)
        self.timestamp = datetime.now().strftime("%d/%m/%Y %H:%M:%S")

    def curate_project(self, project):
        """Curate a project."""
        for p_lu in PROJECT_LIST:
            log.info(f"Exporting {p_lu}...")

            try:
                p = self.client.lookup(p_lu)
            except ApiException as exc:
                log.error("Error getting project: {} - Skipping".format(exc))
                self.reporter.append_log(project_lu=p_lu, state="skipped")
                continue

            payload = PAYLOAD.copy()
            payload["label"] = f"Export triggered by {__name__} -  {self.timestamp}"
            payload["refs"]["project"] = p.id
            payload["refs"]["storage"] = self.storage_id

            export = self.core_client.post("/xfer/exports", json=payload)

            self.reporter.append_log(
                project_lu=p_lu,
                state="submitted",
                project_id=p.id,
                storage_id=self.storage_id,
                export_id=export["_id"],
                export_label=export["label"],
                created=export["created"],
            )
