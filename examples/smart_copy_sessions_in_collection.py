"""A HierarchyCurator script to create a smart copies project from a collection.

This script is intended to alleviate the current limitation with smart copy
only copying to a new project and not supporting collection. It is tailored to copy
sessions from a collection to a single project.

Requirements:
    * subject labels in the collections don't collide.

Usage:
    1. Create an empty project where you want to copy the sessions to and upload
       this script as an attachment to the project.
    2. Modify the below global variables according to your setup in the
       "CONFIGURATION SECTION" of the script:
        * `SOURCE_COLLECTION_ID` (required)
        * `TMP_GROUP` (required)
        * `COPY_WAIT_TIMEOUT` (optional)
    3. Run the script as a HierarchyCurator gear job at the project level from
       the project where you wants to copy the sessions from the collection to.

Notes:
    The script identifies the sessions to be copied and tag them and their
    parents accordingly, then smart copy the sessions to temporary projects and move
    those sessions to the final destination project before cleaning up the temporary
    projects and tags.
"""

import logging
import sys
import time
from functools import lru_cache
from typing import Any, Dict, Optional

import flywheel
from flywheel_gear_toolkit.utils.curator import GearToolkitContext, HierarchyCurator

log = logging.getLogger()

################################################################################
#      START OF THE CONFIGURATION SECTION
################################################################################

# Collection ID to be smart copied
SOURCE_COLLECTION_ID = "64249898c2eefb6b459dc2dc"

# Group where temporary copies will be created
# IMPORTANT: You need to have permission to create project in that group
TMP_GROUP = "flywheel"

# Maximum time to wait for a copy to complete
COPY_WAIT_TIMEOUT = 2 * 3600  # Timeout

# Temporary tag added to session to be smart copied
TMP_TAG_TO_USE = SOURCE_COLLECTION_ID

################################################################################
#      END OF THE CONFIGURATION SECTION
################################################################################


def get_or_create_group(client: flywheel.Client, group_id=None) -> flywheel.Group:
    """Returns or create the group with the given id."""
    try:
        group = client.lookup(group_id)
    except flywheel.ApiException as exc:
        if exc.status == 404:
            client.add_group(group_id)
            group = client.lookup(group_id)
        else:
            log.error("Error creating or getting temporary group: {}".format(exc))
            sys.exit(-1)
    return group


@lru_cache
def add_tag_subject(client, subject_id, tag):
    """Add tag to subject if not already present.

    Args:
        client (flywheel.Client): Flywheel client
        subject_id (str): Subject id
        tag (str): Tag to add

    Returns:
        None
    """
    container = client.get_subject(subject_id)
    if tag not in container.tags:
        container.add_tag(tag)


@lru_cache
def remove_tag_subject(client, subject_id, tag):
    """Remove tag from subject if present."""
    container = client.get_subject(subject_id)
    if tag in container.tags:
        container.delete_tag(tag)


def clean_up(client, copy_projects, col_id, tag):
    """Clean up temporary projects and tags.""."""
    # Delete temporary projects
    for project in copy_projects:
        client.delete_project(project.id)

    # Delete temporary tag
    col = client.get_collection(col_id)
    for s in col.sessions.iter():
        if tag in s.tags:
            s.delete_tag(tag)
            remove_tag_subject(client, s.subject.id, tag)


def tag_sessions_from_collection(client, col_id, tag):
    """Tags sessions and parent according to collection and returns source project ids.

    The need for tagging the parent is to be able to filter on the parent when smart
    copying, otherwise the empty parent will be copied as well.
    """
    col = client.get_collection(col_id)
    project_ids = set()
    for s in col.sessions.iter():
        if s.parents.project not in project_ids:
            project_ids.add(s.parents.project)

        if tag not in s.tags:
            s.add_tag(tag)
            add_tag_subject(client, s.subject.id, tag)

    return project_ids


class Curator(HierarchyCurator):
    """Curator for aggregating smart copies of a projects into a single project."""

    def __init__(
        self, context: GearToolkitContext = None, **kwargs: Optional[Dict[str, Any]]
    ) -> None:
        """Initialize the curator."""
        super().__init__(context, **kwargs)
        if "dst_project" not in kwargs:
            self.dst_project = context.get_destination_parent()
        self.source_proj = []
        self.config.stop_level = "project"
        self.tmp_group = get_or_create_group(self.client, TMP_GROUP)

    def curate_project(self, project):
        """Curate a project."""
        copy_rsp = []
        project_ids = tag_sessions_from_collection(
            self.client, SOURCE_COLLECTION_ID, TMP_TAG_TO_USE
        )
        source_projects = [self.client.get_project(p_id) for p_id in project_ids]
        for project in source_projects:
            log.info(
                f"Triggering smart copy of project {project.group}/{project.label}"
            )

            if project.copyable is False:
                project.update(copyable=True)

            tmp_project_label = f"{project.label}_tmp_copy"

            copy_rsp.append(
                self.client.project_copy(
                    project.id,
                    {
                        "group_id": TMP_GROUP,
                        "project_label": tmp_project_label,
                        "filter": {
                            "include_rules": [
                                f"session.tags={TMP_TAG_TO_USE}",
                                f"subject.tags={TMP_TAG_TO_USE}",
                            ],
                            "exclude_analysis": True,
                            "exclude_notes": True,
                            "exclude_tags": True,
                        },
                    },
                )
            )

        # wait for all copies to complete
        log.info("Waiting for all copies to complete")

        copy_projects = [self.client.get_project(r["project_id"]) for r in copy_rsp]
        pending_copy_complete = [r["project_id"] for r in copy_rsp]

        start_time = time.time()
        while len(pending_copy_complete) > 0:
            time.sleep(5)
            for i, project_id in enumerate(pending_copy_complete):
                project = self.client.get_project(project_id)
                if project.copy_status == "completed":
                    pending_copy_complete.pop(i)
                    log.info(f"Copy project {project.label} complete")
                elif project.copy_status == "failed":
                    log.error(f"Copy project {project.label} failed. Clean and exit...")
                    clean_up(
                        self.client, copy_projects, SOURCE_COLLECTION_ID, TMP_TAG_TO_USE
                    )
                    sys.exit(-1)

            if time.time() - start_time > COPY_WAIT_TIMEOUT:
                log.error("Exceed timeout for copies to complete")
                sys.exit(-1)

        # Move sessions to destination project
        for project in copy_projects:
            log.info(
                f"Moving sessions from {project.label} to {self.dst_project.label}"
            )
            self.client.bulk_move_sessions(
                {
                    "destination_container_type": "projects",
                    "destinations": [self.dst_project.id],
                    "sources": [s.id for s in project.sessions.iter()],
                    "conflict_mode": "move",
                    "remove_source": False,
                }
            )

        log.info("Cleaning up temporary copied projects and tags")
        clean_up(self.client, copy_projects, SOURCE_COLLECTION_ID, TMP_TAG_TO_USE)

        log.info("DONE")
        sys.exit(0)
