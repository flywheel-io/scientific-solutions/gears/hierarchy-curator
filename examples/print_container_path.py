"""An example curation script to print container paths."""

import logging

import flywheel
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger("print_container_path")
log.setLevel("DEBUG")

SPECIAL_LABEL = "my_special_label"


def print_path(levels):
    """Print the path to the container."""
    log.info("/".join(levels))


class Curator(HierarchyCurator):
    """Walk entire hierarchy, but skip everything under a specific subject."""

    def __init__(self, **kwargs):
        """Initialize the curator."""
        super().__init__(**kwargs)
        # By setting the callback to `self.validate_container`
        #   we tell the walker to call this whenever it's going
        #   to queue up a container's children.  `self.validate_container`
        #   returns `True` by default, unless one of the
        #   `self.validate_<container> methods has been implemented.  In this
        #   case we have implemented `self.validate_subject`, so when this
        #   function returns `True`, the walker will queue that subject's
        #   children, when it returns `False` it will not, effectively removing
        #   a branch of the Flyhweel Hierarchy Tree.
        self.config.callback = self.validate_container

    def curate_project(self, project: flywheel.Project):
        """Curate a project."""
        self.project_label = project.label
        print_path([self.project_label])

    def validate_subject(self, session: flywheel.Subject):
        """Validate a subject."""
        if session.label == SPECIAL_LABEL:
            return False
        return True

    def curate_subject(self, subject: flywheel.Subject):
        """Curate a subject."""
        self.subject_label = subject.label
        print_path([self.project_label, self.subject_label])

    def curate_session(self, session: flywheel.Session):
        """Curate a session."""
        self.session_label = session.label
        print_path([self.project_label, self.subject_label, self.session_label])

    def curate_acquisition(self, acquisition: flywheel.Acquisition):
        """Curate an acquisition."""
        print_path(
            [
                self.project_label,
                self.subject_label,
                self.session_label,
                acquisition.label,
            ]
        )
