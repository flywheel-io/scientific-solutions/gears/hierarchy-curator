"""Curation script to add a tag with the file orientation (axial, sagittal, coronal).

It extracts the information from the `file.info.header.dicom.ImageOrientationPatient`
or from the Shared Functional Groups Sequence (for multi-frame DICOMs), so the
`file-metadata-importer` gear needs to be have been run on the files first.
"""

import logging
from numbers import Number
from typing import Optional, Tuple

import flywheel
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger(__name__)


def extract_image_orientation_patient(
    dicom_header: dict,
) -> Optional[Tuple[Number, Number, Number, Number, Number, Number]]:
    """Extract the Image Orientation (Patient) DICOM tag.

    It extracts it from either the top level of the header dictionary or from the
    Shared Functional Groups Sequence (for multi-frame DICOMs).

    Args:
        dicom_header (dict): The DICOM header dictionary.

    Returns:
        Tuple[Number, Number, Number, Number, Number, Number]: The Image Orientation
    """
    if not dicom_header:
        raise ValueError("DICOM header information not present.")

    if image_orientation := dicom_header.get("ImageOrientationPatient", None):
        # single-frame DICOM
        return image_orientation

    if sfgs := dicom_header.get("SharedFunctionalGroupsSequence", []):
        # multi-frame DICOM:
        if plane_orientation_sequence := sfgs[0].get("PlaneOrientationSequence", []):
            unique_image_orientations = set(
                tuple(iop)
                for orientation in plane_orientation_sequence
                if (iop := orientation.get("ImageOrientationPatient", None))
            )
            if len(unique_image_orientations) > 1:
                log.warning(
                    "Found more than one unique Image Orientation (Patient) "
                    "in the Plane Orientation Sequence. Cannot determine a unique Image Orientation."
                )
                return
            if len(unique_image_orientations) == 1:
                # get the only element in the set:
                [only_set_element] = unique_image_orientations
                return only_set_element

    # if we get here, we didn't find the Image Orientation (Patient) tag:
    log.info("Image Orientation (Patient) information not found.")
    return


def extract_orientation_label(
    orientation_tuple: Tuple[Number, Number, Number, Number, Number, Number],
) -> str:
    """Extract the orientation "label" from an orientation Tuple.

    It takes the cross product of the first and second vector of the tuple and finds
    the largest (in absolute value) component of the resulting vector. If the largest
    element is the first/second/third, then the orientation is sagittal/coronal/axial.

    Args:
        orientation_tuple (Tuple[Number, Number, Number, Number, Number, Number]):
            The orientation tuple, consisting on the director cosines along the row
            and column of the image.

    Returns:
        str: The orientation string.
    """
    if (tuple_len := len(orientation_tuple)) != 6:
        raise ValueError(
            f"Invalid orientation tuple. It needs to have exactly 6 elements; "
            f"{tuple_len} provided."
        )

    row_vector = orientation_tuple[:3]
    col_vector = orientation_tuple[3:]
    cross_product = [
        row_vector[1] * col_vector[2] - row_vector[2] * col_vector[1],
        row_vector[2] * col_vector[0] - row_vector[0] * col_vector[2],
        row_vector[0] * col_vector[1] - row_vector[1] * col_vector[0],
    ]
    max_abs_value = max(cross_product, key=abs)  # it can return a negative value
    # if none of the values is larger than 0.8, then the orientation is oblique.
    # This is a more or less arbitrary value...
    if abs(max_abs_value) < 0.8:
        return "oblique"
    # otherwise, determine if it is sagittal, coronal or axial:
    max_index = cross_product.index(max_abs_value)
    if max_index == 0:
        return "sagittal"
    elif max_index == 1:
        return "coronal"
    else:
        return "axial"


class Curator(HierarchyCurator):
    """Hierarchy curator class that adds a tag to a file with the image orientation."""

    def validate_file(self, file_: flywheel.FileEntry) -> bool:
        """Validate a file.

        This method will check if the file is a DICOM file.

        Args:
            file_ (flywheel.FileEntry): The file to be validated.

        Returns:
            bool: True if the file is a DICOM file, False otherwise.
        """
        if file_.type != "dicom":
            return False
        if not file_.info.get("header", {}).get("dicom"):
            log.info(
                "File '%s' (id: %s) does not have the dicom header information "
                "extracted. Please run the 'file-metadata-importer'. "
                "Skipping curation.",
                file_.name,
                file_.file_id,
            )
            return False
        # all good:
        return True

    def curate_file(self, file_: flywheel.FileEntry):
        """Curation method.

        This specific curation method will add a tag with the file orientation.

        Args:
            file_ (flywheel.FileEntry): The file to be curated.
        """
        # Get the Image Orientation (Patient) DICOM tag:
        if not (
            image_orientation := extract_image_orientation_patient(
                file_.info.get("header", {}).get("dicom", {})
            )
        ):
            log.info(
                "DICOM header information not found. Skipping curation. "
                "File '%s' (id: %s)",
                file_.name,
                file_.file_id,
            )
            return

        try:
            tag = extract_orientation_label(image_orientation)
        except ValueError as e:
            log.error(e)
            log.info(
                "Skipping curation for file '%s' (id: %s)", file_.name, file_.file_id
            )
            return

        # Add the orientation as a tag to the file:
        log.info(
            "Adding tag '%s' to file '%s' (id: %s)", tag, file_.name, file_.file_id
        )
        try:
            # add_tag returns the list with the current tags
            tags = file_.add_tag(tag)
        except Exception as e:
            # just log an error, but continue curating other files
            log.error(e)
            return
        # Confirm that the tag was added by checking if it is in the list of tags:
        if tag not in tags:
            log.error(
                "Tag '%s' was not added to file '%s' (id: %s)",
                tag,
                file_.name,
                file_.file_id,
            )
            return
